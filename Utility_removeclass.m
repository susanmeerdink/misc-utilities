function [ spectra, metadata ] = Utility_removeclass( spectra, metadata, classes )
% Removes a class or multiple classes from spectral library and metadata
% before DR and Classification
% Susan Meerdink
% 3/17/2018
% INPUTS:
% 1) spectra: table containing spectra will the same number of rows as
%    metadata in the same order.
% 2) metdata: table containing metadata will the same number of rows as
%    spectra in the same order.
% 3) classes: a cell array that contains the class names to be removed from
%    spectra and metadata.
% 
% OUTPUTS:
% 1) spectra: new table with the classes removed
% 2) metadata: new table with the classes removed

%% Remove Classes
column = strmatch('Dominant', metadata.Properties.VariableNames);

% Loop through classes to be removed
for i = 1: size(classes,2)
    index = strmatch(classes(i), table2cell(metadata(:,column)));
    spectra(index,:) = [];
    metadata(index,:) = [];
end

end

