function [ avgSpectra, stdSpectra, minSpectra, maxSpectra, medSpectra ] = Function_avgdata( data, ids )
% Function that averages data for a species
% Inputs:
% 1) data: matrix containing reflectance or emissivity values for all samples (size allsamples x wavelengths)
% 2) ids: cell array that is samples x 1 with species string code. MUST have same order as data 
%
% Outputs:
% 1) avgSpectra: matrix of spectra averaged based on ids grouping
% 2) stdSpectra: matrix of standard deviation based on ids grouping
% 3) minSpectra: matrix of spectra minimum based on ids grouping
% 4) maxSpectra: matrix of spectra maximum based on ids grouping
% 5) medSpectra: matrix of spectra median based on ids grouping

species = unique(ids);

% Find average/min/max/std of species
avgSpectra = zeros(size(species,1), size(data,2));
stdSpectra = zeros(size(species,1), size(data,2));
minSpectra = zeros(size(species,1), size(data,2));
maxSpectra = zeros(size(species,1), size(data,2));
medSpectra = zeros(size(species,1), size(data,2));

for a = 1:size(species,1)
    spectra = data(strcmp(ids,species(a)),:);
    avgSpectra(a,:) = mean(spectra);
    stdSpectra(a,:) = std(spectra);
    minSpectra(a,:) = min(spectra);
    maxSpectra(a,:) = max(spectra);
    medSpectra(a,:) = median(spectra);
end

end

